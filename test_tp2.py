from boite import *

def test_creation_boite():
    B = Box()
    B.add("1")
    B.add("1")
    B.add("2")
    B.sup("2")
    assert ("1" in B)
    assert ("4" not in B)
    assert ("2" not in B)
    B.ouvert()
    B.is_open()
    assert (B.action_look()=="la boite contient: 1, 1")

    B1 = Box()
    B1.thing(3)
    assert(B1.volume(3)==3)
    B1.set_capacity(5)
    assert (B1.capacity()==5)
    assert (B1.capacity()!=None)
    assert (B1.has_room_for(4))
    assert (not (B1.has_room_for(6)))
    assert (B1.action_add(1))
    assert (B1.capacity()==4)
    assert (B1.capacity()!=3)


